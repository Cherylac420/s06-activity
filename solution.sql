 /*
      a. List books Authored by Marjorie Green

         BU1032  - 'The Busy Executives Database Guide'
         BU2075  - 'You Can Combat Computer Stress'

      b. List the books Authored by Michael O'Leary
       
         BU1111 - 'Cooking with Computers'
     
      c. Write the author/s of "The Busy Executives Database Guide"
      
         213-46-8915 - 'Marjorie Green'
         409-56-7008 - 'Abraham Bennet'

      d. Identify the publisher of "But is it User Friendly"
        
         1389 - 'Algodata Infosystem'

      e. List the books published by Algodata Infosystem
      
         PC1035 - 'The Busy Executives Database Guide'
         BU1032 - 'Cooking with Computers'
         BU1111 - 'Straight Talk About Computers'
         BU7832 - 'But is it User Friendly'
         PC8888 - 'Secrets of Silicon Valley'
         PC9999 - 'Net Etiquette'
*/



-- Create/add a database
CREATE DATABASE blog_db;

-- Select/Use a database;
USE blog_db;

-- Create/add a database

CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_create DATETIME NOT NULL,
PRIMARY KEY(id)
);

-- Create table for post

CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_author_id
FOREIGN KEY(author_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);

-- Create table for post_comments foreign key

CREATE TABLE post_comments(

id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_commented DATETIME NOT NULL,
content VARCHAR(5000),
PRIMARY KEY(id),
CONSTRAINT fk_post_id
FOREIGN KEY(post_id) REFERENCES posts(id)
ON UPDATE CASCADE
ON DELETE RESTRICT,
CONSTRAINT fk_user_id
FOREIGN KEY(user_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);

-- Create table for post_likes foreign key
CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY(id),
CONSTRAINT fk_postlikes_id
FOREIGN KEY(post_id) REFERENCES posts(id)
ON UPDATE CASCADE
ON DELETE RESTRICT,
CONSTRAINT fk_userlikes_id
FOREIGN KEY(user_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);